package com.atlassian.velocity.htmlsafe.introspection;

import java.util.Collections;

import org.apache.velocity.util.introspection.VelMethod;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

/**
 * Tests for the UnboxingMethod
 */
@Ignore
@RunWith(MockitoJUnitRunner.class)
public class TestUnboxingMethod {

    @Mock
    private VelMethod mockVelMethod;

    @InjectMocks
    private VelMethod unboxingMethod;

    @Test
    public void testMethodInvocationUnboxesTargetAndParams() throws Exception {
        Object targetObject = new Object();
        Object boxedTarget = new SimpleBoxedValue(targetObject);
        Object argumentObject = new Object();
        Object[] boxedArguments = new Object[] {new SimpleBoxedValue(argumentObject)};
        Object returnValue = new Object();
        when(mockVelMethod.invoke(targetObject, new Object[] {argumentObject})).thenReturn(returnValue);

        Object methodReturnValue = unboxingMethod.invoke(boxedTarget, boxedArguments);

        assertEquals("Correct value returned", returnValue, methodReturnValue);
    }

    @Test
    public void testMethodInvocationProcessesListsForBoxedElements() throws Exception {
        Object boxedListArgumentMember = new Object();
        Object listArgumentMember = new SimpleBoxedValue(boxedListArgumentMember);
        Object listArgument = Collections.singletonList(listArgumentMember);
        Object originalListArgument = Collections.singletonList(listArgumentMember);
        Object expectedListArgument = Collections.singletonList(boxedListArgumentMember);
        Object[] boxedArguments = new Object[] {listArgument};
        Object targetObject = new Object();
        Object returnValue = new Object();
        when(mockVelMethod.invoke(targetObject, new Object[] {expectedListArgument}))
                .thenReturn(returnValue);

        Object methodReturnValue = unboxingMethod.invoke(targetObject, boxedArguments);

        assertEquals("Correct value returned", returnValue, methodReturnValue);
        assertEquals("Original argument is not modified", originalListArgument, listArgument);
    }
}

package com.atlassian.velocity.htmlsafe.introspection;

import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;

import com.atlassian.velocity.htmlsafe.annotations.CollectionInheritable;
import com.atlassian.velocity.htmlsafe.annotations.ReturnValueAnnotation;

/**
 * Holder for annotations used by the annotation tests
 */
public interface Annotations {
    @TestAnnotation
    @TestReturnValueAnnotation
    @AnotherTestReturnValueAnnotation
    @CollectionInheritableAnnotation
    public static final class AnnotationHolder {
        public static final Annotation TEST_ANNOTATION = AnnotationHolder.class.getAnnotation(TestAnnotation.class);
        public static final Annotation TEST_RETURN_VALUE_ANNOTATION =
                AnnotationHolder.class.getAnnotation(TestReturnValueAnnotation.class);
        public static final Annotation ANOTHER_TEST_RETURN_VALUE_ANNOTATION =
                AnnotationHolder.class.getAnnotation(AnotherTestReturnValueAnnotation.class);
        public static final Annotation COLLETION_RETURN_VALUE_ANNOTATION =
                AnnotationHolder.class.getAnnotation(CollectionInheritableAnnotation.class);
    }

    @Retention(java.lang.annotation.RetentionPolicy.RUNTIME)
    @interface TestAnnotation {}

    @Retention(java.lang.annotation.RetentionPolicy.RUNTIME)
    @interface AnotherTestAnnotation {}

    @Retention(java.lang.annotation.RetentionPolicy.RUNTIME)
    @interface YetAnotherTestAnnotation {}

    @Retention(java.lang.annotation.RetentionPolicy.RUNTIME)
    @ReturnValueAnnotation
    @CollectionInheritable
    @interface CollectionInheritableAnnotation {}

    @Retention(java.lang.annotation.RetentionPolicy.RUNTIME)
    @ReturnValueAnnotation
    @interface TestReturnValueAnnotation {}

    @Retention(java.lang.annotation.RetentionPolicy.RUNTIME)
    @ReturnValueAnnotation
    @interface AnotherTestReturnValueAnnotation {}
}

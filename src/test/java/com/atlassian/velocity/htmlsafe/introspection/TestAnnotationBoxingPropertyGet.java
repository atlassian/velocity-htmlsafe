package com.atlassian.velocity.htmlsafe.introspection;

import java.lang.annotation.Annotation;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

import org.apache.velocity.util.introspection.VelPropertyGet;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

/**
 * Tests for the AnnotationBoxingPropertyGet
 */
@RunWith(MockitoJUnitRunner.class)
public class TestAnnotationBoxingPropertyGet {

    @Mock
    private VelPropertyGet mockVelPropertyGet;

    private static final Collection<Annotation> TEST_ANNOTATIONS =
            Collections.singletonList(Annotations.AnnotationHolder.TEST_ANNOTATION);

    @Test
    public void testMethodInvocationIsBoxed() throws Exception {
        VelPropertyGet boxingPropertyGet = new AnnotationBoxingPropertyGet(mockVelPropertyGet, TEST_ANNOTATIONS);

        Object returnValue = new Object();

        when(mockVelPropertyGet.invoke(any())).thenReturn(returnValue);

        Object getReturnValue = boxingPropertyGet.invoke(new Object());

        Assert.assertTrue("Annotated value returned", getReturnValue instanceof AnnotatedValue);

        AnnotatedValue annotatedReturnValue = (AnnotatedValue) getReturnValue;

        Assert.assertEquals(
                "Return value is correctly annotated",
                TEST_ANNOTATIONS,
                Arrays.asList(annotatedReturnValue.getAnnotations()));
    }

    @Test
    public void testNullIsNotBoxed() throws Exception {
        VelPropertyGet boxingPropertyGet = new AnnotationBoxingPropertyGet(mockVelPropertyGet, TEST_ANNOTATIONS);

        Object methodReturnValue = boxingPropertyGet.invoke(new Object());

        Assert.assertNull("Null return is not boxed", methodReturnValue);
    }
}

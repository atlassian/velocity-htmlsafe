package com.atlassian.velocity.htmlsafe.introspection;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.Collection;

import junit.framework.TestCase;

import com.atlassian.velocity.htmlsafe.HtmlSafe;
import com.atlassian.velocity.htmlsafe.HtmlSafeMethodNameAnnotator;

/**
 * Tests for the HtmlSafeMethodNameAnnotator
 */
public class TestHtmlSafeMethodNameAnnotator extends TestCase {
    public void testRenderMethodsAreMarkedHtmlSafe() throws NoSuchMethodException {
        HtmlSafeMethodNameAnnotator annotator = new HtmlSafeMethodNameAnnotator();
        Method renderMethod = TestClassWithHtmlSafeMethods.class.getMethod("renderMeSomeStuff");

        Collection<Annotation> annotations = annotator.getAnnotationsForMethod(renderMethod);

        assertEquals("Correct number of annotations returned", 1, annotations.size());
        assertEquals(
                "Correct annotation returned",
                HtmlSafe.class,
                annotations.iterator().next().annotationType());
    }

    public void testHtmlMethodsAreMarkedHtmlSafe() throws NoSuchMethodException {
        HtmlSafeMethodNameAnnotator annotator = new HtmlSafeMethodNameAnnotator();
        Method renderMethod = TestClassWithHtmlSafeMethods.class.getMethod("getSomeHtml");

        Collection<Annotation> annotations = annotator.getAnnotationsForMethod(renderMethod);

        assertEquals("Correct number of annotations returned", 1, annotations.size());
        assertEquals(
                "Correct annotation returned",
                HtmlSafe.class,
                annotations.iterator().next().annotationType());
    }

    public void testStandardMethodsAreNotMarkedHtmlSafe() throws NoSuchMethodException {
        HtmlSafeMethodNameAnnotator annotator = new HtmlSafeMethodNameAnnotator();
        Method renderMethod = TestClassWithHtmlSafeMethods.class.getMethod("thisMethodIsNotSafeForHtmlRendering");

        Collection<Annotation> annotations = annotator.getAnnotationsForMethod(renderMethod);

        assertTrue("Method is not marked as HTML safe", annotations.isEmpty());
    }

    private static class TestClassWithHtmlSafeMethods {
        public Object getSomeHtml() {
            return "<html>";
        }

        public String renderMeSomeStuff() {
            return "<b>stuff</b>";
        }

        public Object thisMethodIsNotSafeForHtmlRendering() {
            return "<script>alert('pwnd!');</script>";
        }
    }
}

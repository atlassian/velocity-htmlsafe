package com.atlassian.velocity.htmlsafe.introspection;

import java.lang.annotation.Annotation;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

import org.apache.velocity.util.introspection.VelMethod;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.isNull;

/**
 * Tests for the AnotationBoxingMethod
 */
@RunWith(MockitoJUnitRunner.class)
public class TestAnnotationBoxingMethod {

    @Mock
    private VelMethod mockVelMethod;

    private static final Collection<Annotation> TEST_ANNOTATIONS =
            Collections.singletonList(Annotations.AnnotationHolder.TEST_ANNOTATION);

    @Test
    public void testMethodInvocationReturnIsBoxed() throws Exception {
        VelMethod boxingMethod = new AnnotationBoxingMethod(mockVelMethod, TEST_ANNOTATIONS);
        Mockito.when(mockVelMethod.invoke(any(), isNull())).thenReturn(new Object());

        AnnotatedValue annotatedReturnValue = AnnotatedValue.class.cast(boxingMethod.invoke(new Object(), null));

        Assert.assertEquals(
                "Return value is correctly annotated",
                TEST_ANNOTATIONS,
                Arrays.asList(annotatedReturnValue.getAnnotations()));
    }

    @Test
    public void testNullIsNotBoxed() throws Exception {
        VelMethod boxingMethod = new AnnotationBoxingMethod(mockVelMethod, TEST_ANNOTATIONS);

        Object methodReturnValue = boxingMethod.invoke(new Object(), null);

        Assert.assertNull("Null return is not boxed", methodReturnValue);
    }
}

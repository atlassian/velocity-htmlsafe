package com.atlassian.velocity.htmlsafe.introspection;

import java.lang.annotation.Annotation;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.ExtendedProperties;
import org.apache.commons.lang3.reflect.MethodUtils;
import org.apache.velocity.app.event.EventCartridge;
import org.apache.velocity.runtime.RuntimeConstants;
import org.apache.velocity.runtime.RuntimeServices;
import org.apache.velocity.runtime.log.Log;
import org.apache.velocity.util.introspection.Info;
import org.apache.velocity.util.introspection.SecureUberspector;
import org.apache.velocity.util.introspection.VelMethod;
import org.apache.velocity.util.introspection.VelPropertyGet;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.RestoreSystemProperties;
import org.junit.rules.ExpectedException;
import org.junit.rules.TestName;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import com.google.common.collect.ImmutableList;

import uk.org.lidalia.slf4jext.Level;
import uk.org.lidalia.slf4jtest.LoggingEvent;
import uk.org.lidalia.slf4jtest.TestLogger;
import uk.org.lidalia.slf4jtest.TestLoggerFactory;
import uk.org.lidalia.slf4jtest.TestLoggerFactoryResetRule;

import com.atlassian.velocity.htmlsafe.introspection.deprecation.ClassWithDeprecatedMethod;
import com.atlassian.velocity.htmlsafe.introspection.deprecation.DeprecatedClass;

import static java.util.Arrays.asList;
import static java.util.Collections.singleton;
import static java.util.Collections.singletonList;
import static java.util.Collections.singletonMap;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.AdditionalAnswers.returnsArgAt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import static com.atlassian.velocity.htmlsafe.introspection.Annotations.AnnotationHolder.ANOTHER_TEST_RETURN_VALUE_ANNOTATION;
import static com.atlassian.velocity.htmlsafe.introspection.Annotations.AnnotationHolder.COLLETION_RETURN_VALUE_ANNOTATION;
import static com.atlassian.velocity.htmlsafe.introspection.Annotations.AnnotationHolder.TEST_RETURN_VALUE_ANNOTATION;

/**
 * Tests for the annotation boxing uberspect
 */
@RunWith(MockitoJUnitRunner.class)
public class TestAnnotationBoxingUberspect {
    private AnnotationBoxingUberspect uberspect;
    private Info testInfo;
    private Collection<Annotation> returnValueAnnotations;

    @Mock
    private RuntimeServices mockRuntimeServices;

    private EventCartridge testApplicationEventCartridge;

    @Mock
    private ExtendedProperties extendedProperties;

    @Rule
    public TestName testName = new TestName();

    @Rule
    public RestoreSystemProperties restoreSystemProperties = new RestoreSystemProperties();

    @Rule
    public TestLoggerFactoryResetRule testLoggerFactoryResetRule = new TestLoggerFactoryResetRule();

    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Before
    public void setUp() throws Exception {
        testApplicationEventCartridge = new EventCartridge();
        when(mockRuntimeServices.getApplicationEventCartridge()).thenReturn(testApplicationEventCartridge);
        when(mockRuntimeServices.getConfiguration()).thenReturn(extendedProperties);
        when(extendedProperties.getString(anyString(), anyString())).thenAnswer(returnsArgAt(1));
        when(extendedProperties.getStringArray(RuntimeConstants.INTROSPECTOR_RESTRICT_PACKAGES))
                .thenReturn(new String[] {});
        when(extendedProperties.getStringArray(RuntimeConstants.INTROSPECTOR_RESTRICT_CLASSES))
                .thenReturn(new String[] {});
        when(extendedProperties.getStringArray(RuntimeConstants.INTROSPECTOR_ALLOWLIST_CLASSES))
                .thenReturn(new String[] {});
        when(extendedProperties.getStringArray(RuntimeConstants.INTROSPECTOR_ALLOWLIST_METHODS))
                .thenReturn(new String[] {});
        when(extendedProperties.getStringArray(RuntimeConstants.INTROSPECTOR_ALLOW_PACKAGES))
                .thenReturn(new String[] {});
        when(extendedProperties.getStringArray(RuntimeConstants.INTROSPECTOR_ALLOW_CLASSES))
                .thenReturn(new String[] {});
        when(extendedProperties.getStringArray(RuntimeConstants.INTROSPECTOR_PROPER_ALLOWLIST_METHODS))
                .thenReturn(new String[] {});
        when(extendedProperties.getStringArray(RuntimeConstants.INTROSPECTOR_PROPER_ALLOWLIST_CLASSES))
                .thenReturn(new String[] {});
        when(extendedProperties.getStringArray(RuntimeConstants.INTROSPECTOR_PROPER_ALLOWLIST_PACKAGES))
                .thenReturn(new String[] {});
        uberspect = new AnnotationBoxingUberspect();
        uberspect.setLog(new Log());
        uberspect.setRuntimeServices(mockRuntimeServices);
        uberspect.init();
        testInfo = new Info("unitTest", 1, 1);
        returnValueAnnotations =
                new HashSet<>(asList(TEST_RETURN_VALUE_ANNOTATION, ANOTHER_TEST_RETURN_VALUE_ANNOTATION));
    }

    @Test
    public void testAnnotatedValueHandlerIsRegistered() {
        final Iterator eventHandlerIterator = testApplicationEventCartridge.getReferenceInsertionEventHandlers();
        assertTrue(eventHandlerIterator.hasNext());
        assertTrue(eventHandlerIterator.next() instanceof AnnotatedValueStringHandler);
    }

    @Test
    public void testGetMethodOnMissingMethod() throws Exception {
        final VelMethod method = uberspect.getMethod(new Object(), "missing", new Object[] {}, testInfo);
        assertNull(method);
    }

    @Test
    public void testGetMethodOnNonBoxedValue() throws Exception {
        final Object value = new Object();
        final InvocationDetector detector = new InvocationDetector(value);

        final VelMethod method = uberspect.getMethod(detector, "getObject", new Object[] {}, testInfo);
        assertNotNull(method);
        final Object returnValue = method.invoke(detector, new Object[] {});
        assertEquals(returnValue, value);
    }

    @Test
    public void testGetMethodWithArgsOnNonBoxedValue() throws Exception {
        final Object value = new Object();
        final InvocationDetector detector = new InvocationDetector(value);
        final List arguments = asList(new Object(), new Object());

        final VelMethod method =
                uberspect.getMethod(detector, "recordArguments", new Object[] {new Object(), new Object()}, testInfo);
        assertNotNull(method);
        final Object returnValue = method.invoke(detector, arguments.toArray());
        assertEquals(returnValue, value);
        assertEquals(arguments, detector.callArguments);
    }

    @Test
    public void testGetMethodWithBoxedTarget() throws Exception {
        final Object value = new Object();
        final InvocationDetector detector = new InvocationDetector(value);
        final BoxedValue boxedValue = new SimpleBoxedValue(detector);

        final List arguments = asList(new Object(), new Object());

        final VelMethod method =
                uberspect.getMethod(boxedValue, "recordArguments", new Object[] {new Object(), new Object()}, testInfo);
        assertNotNull(method);
        final Object returnValue = method.invoke(boxedValue, arguments.toArray());
        assertEquals(returnValue, value);
        assertEquals(arguments, detector.callArguments);
    }

    @Test
    public void testGetMethodWithBoxedArguments() throws Exception {
        final Object value = new Object();
        final InvocationDetector detector = new InvocationDetector(value);

        final List arguments = asList(new Object(), new Object());
        final List boxedArguments = asList(arguments.get(0), new SimpleBoxedValue(arguments.get(1)));

        final VelMethod method = uberspect.getMethod(detector, "recordArguments", boxedArguments.toArray(), testInfo);
        assertNotNull(method);
        final Object returnValue = method.invoke(detector, boxedArguments.toArray());
        assertEquals(returnValue, value);
        assertEquals(arguments, detector.callArguments);
    }

    @Test
    public void testGetMethodWithReturnValueAnnotations() throws Exception {
        final Object value = new Object();
        final InvocationDetector detector = new AnnotatedInvocationDetector(value);
        final AnnotatedValue expectedValue = new AnnotatedValue<>(value, returnValueAnnotations);

        final VelMethod method = uberspect.getMethod(detector, "getObject", new Object[] {}, testInfo);
        assertNotNull(method);
        final Object returnValue = method.invoke(detector, new Object[] {});

        assertEquals(expectedValue, returnValue);
    }

    @Test
    public void testGetMethodWithAnnotationInheritingMap() throws Exception {
        final Object value = new Object();
        final InvocationDetector detector = new AnnotatedInvocationDetector(value);
        final Collection<Annotation> expectedAnnotations =
                new HashSet<>(asList(COLLETION_RETURN_VALUE_ANNOTATION, TEST_RETURN_VALUE_ANNOTATION));

        final AnnotatedValue expectedValue = new AnnotatedValue<>(singletonMap("testKey", value), expectedAnnotations);

        final VelMethod method = uberspect.getMethod(detector, "getMap", new Object[] {}, testInfo);
        assertNotNull(method);
        final Object returnMapValue = method.invoke(detector, new Object[] {});
        assertEquals(expectedValue, returnMapValue);

        final AnnotatedValue expectedMapValue =
                new AnnotatedValue<>(value, singleton(COLLETION_RETURN_VALUE_ANNOTATION));

        final VelMethod mapGetMethod = uberspect.getMethod(returnMapValue, "get", new Object[] {"testKey"}, testInfo);
        assertNotNull(mapGetMethod);

        final Object mapValue = mapGetMethod.invoke(expectedValue.unbox(), new Object[] {"testKey"});
        assertEquals(expectedMapValue, mapValue);
    }

    @Test
    public void testGetMethodWithAnnotationInheritingList() throws Exception {
        final Object value = new Object();
        final InvocationDetector detector = new AnnotatedInvocationDetector(value);
        final Collection<Annotation> expectedAnnotations = new HashSet<>(asList(COLLETION_RETURN_VALUE_ANNOTATION));

        final AnnotatedValue expectedValue = new AnnotatedValue<>(singletonList(value), expectedAnnotations);

        final VelMethod method = uberspect.getMethod(detector, "getList", new Object[] {}, testInfo);
        assertNotNull(method);
        final Object returnListValue = method.invoke(detector, new Object[] {});
        assertEquals(expectedValue, returnListValue);

        final AnnotatedValue expectedListValue =
                new AnnotatedValue<>(value, singleton(COLLETION_RETURN_VALUE_ANNOTATION));

        final VelMethod listGetMethod = uberspect.getMethod(returnListValue, "get", new Object[] {0}, testInfo);
        assertNotNull(listGetMethod);

        final Object listValue = listGetMethod.invoke(expectedValue.unbox(), new Object[] {0});
        assertEquals(expectedListValue, listValue);
    }

    @Test
    public void testPropertyGetWithReturnValueAnnotations() throws Exception {
        final Object value = new Object();
        final InvocationDetector detector = new AnnotatedInvocationDetector(value);
        final AnnotatedValue expectedValue = new AnnotatedValue<>(value, returnValueAnnotations);

        final VelPropertyGet getter = uberspect.getPropertyGet(detector, "object", testInfo);
        assertNotNull(getter);
        final Object returnValue = getter.invoke(detector);

        assertEquals(expectedValue, returnValue);
    }

    @Test
    public void testPropertyGetViaGetterMethod() throws Exception {
        final Object value = new Object();
        final InvocationDetector detector = new InvocationDetector(value);

        final VelPropertyGet getter = uberspect.getPropertyGet(detector, "object", testInfo);
        assertNotNull(getter);
        final Object returnValue = getter.invoke(detector);

        assertEquals(value, returnValue);
    }

    @Test
    public void testGetIterator() throws Exception {
        final Object value = new Object();

        final List testList = singletonList(value);

        final Iterator returnedIterator = uberspect.getIterator(testList, testInfo);
        assertNotNull(returnedIterator);
        assertTrue(returnedIterator.hasNext());
        assertEquals(returnedIterator.next(), value);
    }

    @Test
    public void testAnnotationInheritingIterator() throws Exception {
        final Object value = new Object();
        final Collection<Annotation> expectedAnnotations = new HashSet<>(asList(COLLETION_RETURN_VALUE_ANNOTATION));

        final AnnotatedValue annotatedList = new AnnotatedValue<>(singletonList(value), expectedAnnotations);

        final Iterator returnedIterator = uberspect.getIterator(annotatedList, testInfo);
        assertNotNull(returnedIterator);
        assertTrue(returnedIterator.hasNext());

        final AnnotatedValue expectedIteratorMember = new AnnotatedValue<>(value, expectedAnnotations);
        assertEquals(returnedIterator.next(), expectedIteratorMember);
    }

    @Test
    public void buildDeprecatedMethodAccessMessage() {
        final String message = AnnotationBoxingUberspect.buildDeprecatedMethodAccessMessage(
                "", getClass(), MethodUtils.getAccessibleMethod(getClass(), testName.getMethodName()), testInfo);
        assertThat(
                message,
                allOf(
                        containsString(getClass().getName()),
                        containsString(testName.getMethodName()),
                        containsString(testInfo.toString())));
    }

    @Test
    public void normalMethodsTriggerNoExceptionsInStrictMode() throws Exception {
        System.setProperty(AnnotationBoxingUberspect.ATLASSIAN_DEVMODE, "false");
        System.setProperty(AnnotationBoxingUberspect.ATLASSIAN_VELOCITY_DEPRECATION_STRICTMODE, "true");
        VelMethod undeprecatedMethod =
                uberspect.getMethod(new ClassWithDeprecatedMethod(), "undeprecatedMethod", new Class[0], testInfo);
        assertThat(undeprecatedMethod, notNullValue());
    }

    @Test
    public void normalMethodsReturningDeprecatedClassesTriggerNoExceptionsInStrictMode() throws Exception {
        System.setProperty(AnnotationBoxingUberspect.ATLASSIAN_DEVMODE, "false");
        System.setProperty(AnnotationBoxingUberspect.ATLASSIAN_VELOCITY_DEPRECATION_STRICTMODE, "true");
        VelMethod getDeprecatedClass =
                uberspect.getMethod(new ClassWithDeprecatedMethod(), "getDeprecatedClass", new Class[0], testInfo);
        assertThat(getDeprecatedClass, notNullValue());
    }

    @Test
    public void deprecatedMethodsTriggerExceptionsInStrictMode() throws Exception {
        exception.expect(UnsupportedOperationException.class);
        exception.expectMessage(containsString("Velocity template accessing deprecated method"));
        System.setProperty(AnnotationBoxingUberspect.ATLASSIAN_DEVMODE, "false");
        System.setProperty(AnnotationBoxingUberspect.ATLASSIAN_VELOCITY_DEPRECATION_STRICTMODE, "true");
        uberspect.getMethod(new ClassWithDeprecatedMethod(), "deprecatedMethod", new Class[0], testInfo);
    }

    @Test
    public void deprecatedClassesTriggerExceptionsInStrictMode() throws Exception {
        exception.expect(UnsupportedOperationException.class);
        exception.expectMessage(containsString("Velocity template accessing method on deprecated class"));
        System.setProperty(AnnotationBoxingUberspect.ATLASSIAN_DEVMODE, "false");
        System.setProperty(AnnotationBoxingUberspect.ATLASSIAN_VELOCITY_DEPRECATION_STRICTMODE, "true");
        //noinspection deprecation
        uberspect.getPropertyGet(new DeprecatedClass(), "value", testInfo);
    }

    @Test
    public void deprecatedMethodsTriggerLogMessagesOnlyWithStrictModeOff() throws Exception {
        TestLogger log = TestLoggerFactory.getTestLogger(AnnotationBoxingUberspect.class);
        log.setEnabledLevels(Level.DEBUG);
        System.setProperty(AnnotationBoxingUberspect.ATLASSIAN_DEVMODE, "false");
        System.setProperty(AnnotationBoxingUberspect.ATLASSIAN_VELOCITY_DEPRECATION_STRICTMODE, "false");
        VelMethod deprecatedMethod =
                uberspect.getMethod(new ClassWithDeprecatedMethod(), "deprecatedMethod", new Class[0], testInfo);
        assertThat(deprecatedMethod, notNullValue());
        ImmutableList<LoggingEvent> loggingEvents = log.getLoggingEvents();
        assertThat(loggingEvents, hasSize(1));
        assertThat(loggingEvents.get(0).getMessage(), containsString("Velocity template accessing deprecated method"));
    }

    @Test
    public void deprecatedMethodsTriggerWarnLogMessagesOnlyInDevModeWithStrictModeOff() throws Exception {
        TestLogger log = TestLoggerFactory.getTestLogger(AnnotationBoxingUberspect.class);
        log.setEnabledLevels(Level.WARN);
        System.setProperty(AnnotationBoxingUberspect.ATLASSIAN_DEVMODE, "true");
        System.setProperty(AnnotationBoxingUberspect.ATLASSIAN_VELOCITY_DEPRECATION_STRICTMODE, "false");
        VelMethod deprecatedMethod =
                uberspect.getMethod(new ClassWithDeprecatedMethod(), "deprecatedMethod", new Class[0], testInfo);
        assertThat(deprecatedMethod, notNullValue());
        ImmutableList<LoggingEvent> loggingEvents = log.getLoggingEvents();
        assertThat(loggingEvents, hasSize(1));
        assertThat(loggingEvents.get(0).getMessage(), containsString("Velocity template accessing deprecated method"));
    }

    @Test
    public void deprecatedMethodsTriggerExceptionsAndLoggingWithConfluenceStrictModeOn() throws Exception {
        TestLogger log = TestLoggerFactory.getTestLogger(AnnotationBoxingUberspect.class);
        log.setEnabledLevels(Level.WARN);
        System.setProperty(AnnotationBoxingUberspect.ATLASSIAN_DEVMODE, "false");
        System.setProperty(AnnotationBoxingUberspect.LEGACY_CONFLUENCE_VELOCITY_DEPRECATION_STRICTMODE, "true");
        try {
            uberspect.getMethod(new ClassWithDeprecatedMethod(), "deprecatedMethod", new Class[0], testInfo);
            fail("Expected exception");
        } catch (UnsupportedOperationException e) {
            assertThat(e.getMessage(), containsString("Velocity template accessing deprecated method"));
        }
        ImmutableList<LoggingEvent> loggingEvents = log.getLoggingEvents();
        assertThat(loggingEvents, hasSize(1));
        assertThat(loggingEvents.get(0).getMessage(), containsString("System property {} is deprecated"));
    }

    @Test
    public void testAnnotationBoxingUberspectIsSecureUberspector() {
        assertThat(uberspect, instanceOf(SecureUberspector.class));
    }

    public static class InvocationDetector {
        public final Object returnValue;
        private List callArguments;

        protected InvocationDetector(Object returnValue) {
            this.returnValue = returnValue;
        }

        public Object getObject() {
            return returnValue;
        }

        public Map getMap() {
            return singletonMap("testKey", returnValue);
        }

        public List getList() {
            return singletonList(returnValue);
        }

        // used via velocity reflective calls
        @SuppressWarnings("UnusedDeclaration")
        public Object recordArguments(Object arg1, Object arg2) {
            callArguments = asList(arg1, arg2);
            return returnValue;
        }
    }

    public static class AnnotatedInvocationDetector extends InvocationDetector {
        protected AnnotatedInvocationDetector(Object returnValue) {
            super(returnValue);
        }

        @Override
        @Annotations.CollectionInheritableAnnotation
        @Annotations.TestReturnValueAnnotation
        public Map getMap() {
            return super.getMap();
        }

        @Override
        @Annotations.CollectionInheritableAnnotation
        @Annotations.TestAnnotation
        public List getList() {
            return super.getList();
        }

        @Override
        @Annotations.TestReturnValueAnnotation
        @Annotations.YetAnotherTestAnnotation
        @Annotations.AnotherTestReturnValueAnnotation
        @Annotations.TestAnnotation
        public Object getObject() {
            return super.getObject();
        }
    }
}

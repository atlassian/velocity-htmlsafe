package com.atlassian.velocity.htmlsafe.introspection;

import junit.framework.TestCase;

/**
 * Tests for BoxingUtils
 */
public class TestBoxingUtils extends TestCase {
    public void testUnboxBoxedObject() {
        final Object value = new Object();
        final Object boxedValue = new SimpleBoxedValue(value);

        final Object unboxedValue = BoxingUtils.unboxObject(boxedValue);

        assertTrue("Boxed value is unboxed correctly", value.equals(unboxedValue));
    }

    public void testUnboxUnboxedObject() {
        final Object value = new Object();
        final Object unboxedValue = BoxingUtils.unboxObject(value);

        assertTrue("Unboxed value is unboxed correctly", value.equals(unboxedValue));
    }

    public void testNullIsUnboxable() {
        final Object value = null;
        final Object unboxedValue = BoxingUtils.unboxObject(value);

        assertNull("Null is unboxed correctly", unboxedValue);
    }

    public void testArrayWithBoxedValueIsUnboxedCorrectly() {
        final Object value1 = new Object();
        final Object value2 = new Object();
        final Object value3 = new Object();

        final Object[] boxedValueArray =
                new Object[] {new SimpleBoxedValue(value1), value2, new SimpleBoxedValue(value3)};
        final Object[] unboxedValueArray = BoxingUtils.unboxArrayElements(boxedValueArray);

        assertEquals("Unboxed array size is preserved", 3, unboxedValueArray.length);
        assertEquals("Array values are unboxed correctly", value1, unboxedValueArray[0]);
        assertEquals("Unboxed array values are unboxed correctly", value2, unboxedValueArray[1]);
        assertEquals("Array values are unboxed correctly", value3, unboxedValueArray[2]);
    }
}

package com.atlassian.velocity.htmlsafe.introspection;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Tests for the ToStringDelegatingAnnotationBoxedElement
 */
public class TestToStringDelegatingAnnotationBoxedElement {
    @Test
    public void testToStringIsDelegated() {
        ToStringTestInterface mockValue = mock(ToStringTestInterface.class);

        Object testValue = mockValue;

        AnnotationBoxedElement mockAnnotationBoxedElement = mock(AnnotationBoxedElement.class);

        when(mockAnnotationBoxedElement.unbox()).thenReturn(testValue);
        final String expectedString = String.valueOf(System.identityHashCode(testValue));
        when(mockValue.toString()).thenReturn(expectedString);

        AnnotationBoxedElement boxedElement = mockAnnotationBoxedElement;
        ToStringDelegatingAnnotationBoxedElement delegatingElement =
                new ToStringDelegatingAnnotationBoxedElement(boxedElement);

        String resultString = delegatingElement.toString();
        assertNotNull(resultString);
        assertEquals(expectedString, resultString);
    }

    @Test
    public void testBoxingStrategyPreservesToStringDelegation() {
        Object testValue = new Object();

        AnnotationBoxedElement mockAnnotationBoxedElement = mock(AnnotationBoxedElement.class);

        AnnotationBoxedElement mockReboxedElement = mock(AnnotationBoxedElement.class);

        Object reboxedElement = mockReboxedElement;

        when(mockAnnotationBoxedElement.box(testValue)).thenReturn(reboxedElement);
        when(mockReboxedElement.unbox()).thenReturn(testValue);

        AnnotationBoxedElement boxedElement = mockAnnotationBoxedElement;
        ToStringDelegatingAnnotationBoxedElement delegatingElement =
                new ToStringDelegatingAnnotationBoxedElement(boxedElement);

        Object reboxingResult = delegatingElement.box(testValue);
        assertNotNull(reboxingResult);
        assertThat(reboxingResult, instanceOf(ToStringDelegatingAnnotationBoxedElement.class));
        assertEquals(((AnnotationBoxedElement) reboxingResult).unbox(), testValue);
    }

    @Test
    public void testToStringOfBoxedNull() {
        AnnotationBoxedElement mockAnnotationBoxedElement = mock(AnnotationBoxedElement.class);
        ToStringDelegatingAnnotationBoxedElement element =
                new ToStringDelegatingAnnotationBoxedElement(mockAnnotationBoxedElement);

        when(mockAnnotationBoxedElement.unbox()).thenReturn(null);
        assertEquals("null", element.toString());
    }

    private static interface ToStringTestInterface {
        String toString();
    }
}

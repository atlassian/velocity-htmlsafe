package com.atlassian.velocity.htmlsafe.introspection;

import java.lang.annotation.Annotation;
import java.util.Collections;
import java.util.Set;

import junit.framework.TestCase;

import static com.google.common.collect.Iterables.elementsEqual;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Tests for the AnnotatedValueHelperFactory
 */
public class TestAnnotatedValueHelperFactory extends TestCase {
    public void testNonAnnotatedValueHelper() {
        Object value = new Object();
        ObjectClassResolver mockObjectClassResolver = mock(ObjectClassResolver.class);
        AnnotatedValueHelper valueHelper =
                new AnnotatedValueHelperFactory.NonAnnotatedValueHelper(value, mockObjectClassResolver);

        assertEquals(value, valueHelper.get());
        assertEquals(value, valueHelper.unbox());
        assertTrue(valueHelper.getAnnotations().isEmpty());
        assertNull(valueHelper.getBoxedValueWithInheritedAnnotations());

        when(mockObjectClassResolver.resolveClass(value)).thenReturn(Object.class);
        assertEquals(Object.class, valueHelper.getTargetClass());
    }

    public void testDefaultAnnotatedValueHelperWithAnnotatedElement() {
        Object value = new Object();

        AnnotationBoxedElement mockAnnotationBoxedElement = mock(AnnotationBoxedElement.class);

        when(mockAnnotationBoxedElement.unbox()).thenReturn(value);
        final Set<Annotation> expectedAnnotations = Collections.singleton(Annotations.AnnotationHolder.TEST_ANNOTATION);
        when(mockAnnotationBoxedElement.getAnnotations()).thenReturn(expectedAnnotations.toArray(new Annotation[1]));

        ObjectClassResolver mockObjectClassResolver = mock(ObjectClassResolver.class);
        AnnotatedValueHelper valueHelper = new AnnotatedValueHelperFactory.DefaultAnnotatedValueHelper(
                mockAnnotationBoxedElement, mockObjectClassResolver);

        assertEquals(mockAnnotationBoxedElement, valueHelper.get());
        assertEquals(value, valueHelper.unbox());
        assertTrue(elementsEqual(expectedAnnotations, valueHelper.getAnnotations()));
        // assertNull(valueHelper.getBoxedValueWithInheritedAnnotations());

        when(mockObjectClassResolver.resolveClass(value)).thenReturn(Object.class);
        assertEquals(Object.class, valueHelper.getTargetClass());
    }
}

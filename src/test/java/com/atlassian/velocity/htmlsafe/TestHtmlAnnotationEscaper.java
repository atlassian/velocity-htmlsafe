package com.atlassian.velocity.htmlsafe;

import java.util.Collections;

import junit.framework.TestCase;

import com.atlassian.velocity.htmlsafe.introspection.AnnotatedValue;
import com.atlassian.velocity.htmlsafe.introspection.Annotations;
import com.atlassian.velocity.htmlsafe.introspection.Annotations.AnnotationHolder;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.isA;

/**
 * Tests for the HtmlAnnotationEscaper
 */
public class TestHtmlAnnotationEscaper extends TestCase {
    public void testNullValuesAreHandled() {
        HtmlAnnotationEscaper escaper = new HtmlAnnotationEscaper();
        Object returnValue = escaper.referenceInsert("$myNullValue", null);
        assertNull("Nulls are handled", returnValue);
    }

    public void testHtmlReferenceNamesAreNotEscaped() {
        HtmlAnnotationEscaper escaper = new HtmlAnnotationEscaper();
        Object returnValue = escaper.referenceInsert("$thisIsHtml", "<em>this is &lt;markup&gt;</em>");
        assertEquals("Return value is not escaped", "<em>this is &lt;markup&gt;</em>", returnValue);
    }

    public void testBracedHtmlReferenceNamesAreNotEscaped() {
        HtmlAnnotationEscaper escaper = new HtmlAnnotationEscaper();
        Object returnValue = escaper.referenceInsert("${thisIsHtml}", "<em>this is &lt;markup&gt;</em>");
        assertEquals("Return value is not escaped", "<em>this is &lt;markup&gt;</em>", returnValue);
    }

    public void testNonHtmlReferenceNamesAreEscaped() {
        HtmlAnnotationEscaper escaper = new HtmlAnnotationEscaper();
        Object returnValue = escaper.referenceInsert("$thisIsNotSafe", "<em>this is not safe &lt;markup&gt;</em>");
        assertEquals(
                "Return value is escaped", "&lt;em&gt;this is not safe &amp;lt;markup&amp;gt;&lt;/em&gt;", returnValue);
    }

    public void testObjectsWithHtmlSafeToStringMethodAreNotEscaped() {
        HtmlAnnotationEscaper escaper = new HtmlAnnotationEscaper();
        HtmlFragment fragment = new HtmlFragment("<strong>html safe markup</strong>");
        Object returnValue = escaper.referenceInsert("$someRef", fragment);
        assertEquals("Object with @HtmlSafe toString method is not escaped", fragment, returnValue);
    }

    public void testObjectsWithHtmlSafeAnnotationAreNotEscaped() {
        HtmlAnnotationEscaper escaper = new HtmlAnnotationEscaper();
        String htmlString = "<strong>more html safe markup</strong>";

        AnnotatedValue htmlSafeValue =
                new AnnotatedValue<>(htmlString, Collections.singleton(HtmlSafeAnnotationUtils.HTML_SAFE_ANNOTATION));

        Object returnValue = escaper.referenceInsert("$someRef", htmlSafeValue);
        assertEquals("Object with associated @HtmlSafe annotation is not escaped", htmlSafeValue, returnValue);
    }

    public void testObjectsWithoutHtmlSafeAnnotationsAreEscaped() {
        HtmlAnnotationEscaper escaper = new HtmlAnnotationEscaper();
        String htmlString = "<script>alert('hello')</script>";

        AnnotatedValue htmlUnsafeValue =
                new AnnotatedValue<>(htmlString, Collections.singleton(AnnotationHolder.TEST_ANNOTATION));
        AnnotatedValue escapedValue = new AnnotatedValue<>(
                "&lt;script&gt;alert(&#39;hello&#39;)&lt;/script&gt;",
                Collections.singleton(Annotations.AnnotationHolder.TEST_ANNOTATION));

        Object returnValue = escaper.referenceInsert("$someOtherRef", htmlUnsafeValue);
        assertEquals("Object with no @HtmlSafe annotation is escaped", escapedValue, returnValue);
    }

    public void testSpecialCaseReferencesToBodyAndHeadAreNotEscaped() {
        HtmlAnnotationEscaper escaper = new HtmlAnnotationEscaper();

        Object originalValue = new Object();

        assertSame(originalValue, escaper.referenceInsert("$body", originalValue));
        assertSame(originalValue, escaper.referenceInsert("$head", originalValue));
    }

    public void testNonSpecialCaseReferencesAreStringified() {
        HtmlAnnotationEscaper escaper = new HtmlAnnotationEscaper();

        Object originalValue = new Object();

        assertThat(escaper.referenceInsert("$test", originalValue), isA((Class) String.class));
    }
}

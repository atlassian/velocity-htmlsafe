package com.atlassian.velocity.htmlsafe;

import junit.framework.TestCase;

/**
 * Tests for HtmlFragment
 */
public class TestHtmlFragment extends TestCase {
    public void testToStringIsDelegated() {
        Object value = new Object();
        HtmlFragment fragment = new HtmlFragment(value);

        assertEquals(value.toString(), fragment.toString());
    }

    public void testToStringIsHtmlSafe() {
        Object value = new Object();
        HtmlFragment fragment = new HtmlFragment(value);
        assertTrue(HtmlSafeAnnotationUtils.hasHtmlSafeToStringMethod(fragment));
    }

    public void testUnboxReturnsBoxedValue() {
        Object value = new Object();
        HtmlFragment fragment = new HtmlFragment(value);

        assertEquals(value, fragment.unbox());
    }
}

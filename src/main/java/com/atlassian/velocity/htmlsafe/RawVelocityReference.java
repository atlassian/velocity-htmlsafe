package com.atlassian.velocity.htmlsafe;

/**
 * Represents a raw velocity template reference such as those passed to
 * {@link org.apache.velocity.app.event.ReferenceInsertionEventHandler}s
 */
final class RawVelocityReference {
    private final String referenceString;

    public RawVelocityReference(String referenceString) {
        if (referenceString == null) {
            throw new NullPointerException("referenceString must not be null");
        }
        this.referenceString = referenceString;
    }

    /**
     * @return true if this reference represents a scalar context value.
     */
    public boolean isScalar() {
        return referenceString.indexOf('.') == -1;
    }

    /**
     * @return the scalar component of the raw reference. If the reference has no non-scalar component, this object
     * is returned.
     */
    public RawVelocityReference getScalarComponent() {
        final int pos = referenceString.indexOf('.');
        return (pos == -1) ? this : new RawVelocityReference(referenceString.substring(0, pos));
    }

    /**
     * Returns the scalar component name of this reference
     * <p>
     * <dl>
     * <dt>$foo</dt>
     * <dd>foo</dd>
     * <dt>${bar}<dt>
     * <dd>bar</dt>
     * <dt>$myVar.someMethod()</dt>
     * <dd>myVar</dd>
     * </dl>
     *
     * @return The scalar component name
     */
    public String getBaseReferenceName() {
        final int len = referenceString.length();
        final StringBuilder str = new StringBuilder(len);
        for (int j = 0; j < len; ++j) {
            final char c = referenceString.charAt(j);
            switch (c) {
                case '!':
                case '$':
                case '{':
                case '}':
                    continue;
                case '.':
                    return str.toString();
                default:
                    str.append(c);
            }
        }
        return str.toString();
    }
}

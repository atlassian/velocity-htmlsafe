package com.atlassian.velocity.htmlsafe.context;

import org.apache.velocity.app.event.EventCartridge;
import org.apache.velocity.app.event.ReferenceInsertionEventHandler;

import com.atlassian.velocity.htmlsafe.PolicyBasedReferenceInsertionHandler;
import com.atlassian.velocity.htmlsafe.ReferenceInsertionPolicy;

/**
 * Adds the necessary event handlers based on the provided {@link ReferenceInsertionPolicy}
 */
public final class InsertionPolicyCartridgeProcessor implements EventCartridgeProcessor {
    private final ReferenceInsertionEventHandler REFERENCE_INSERTION_EVENT_HANDLER;

    public InsertionPolicyCartridgeProcessor(ReferenceInsertionPolicy encodingPolicy) {
        this.REFERENCE_INSERTION_EVENT_HANDLER = new PolicyBasedReferenceInsertionHandler(encodingPolicy);
    }

    public void processCartridge(EventCartridge cartridge) {
        cartridge.addReferenceInsertionEventHandler(REFERENCE_INSERTION_EVENT_HANDLER);
    }
}

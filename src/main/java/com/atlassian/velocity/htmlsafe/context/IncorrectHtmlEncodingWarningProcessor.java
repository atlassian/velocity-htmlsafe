package com.atlassian.velocity.htmlsafe.context;

import org.apache.velocity.app.event.EventCartridge;

import com.atlassian.velocity.htmlsafe.PossibleIncorrectHtmlEncodingEventHandler;

/**
 * Adds a {@link PossibleIncorrectHtmlEncodingEventHandler} to a cartridge if such logging is enabled.
 */
public final class IncorrectHtmlEncodingWarningProcessor implements EventCartridgeProcessor {
    public void processCartridge(EventCartridge cartridge) {
        if (PossibleIncorrectHtmlEncodingEventHandler.isLoggingEnabled())
            cartridge.addEventHandler(new PossibleIncorrectHtmlEncodingEventHandler());
    }
}

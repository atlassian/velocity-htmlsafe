package com.atlassian.velocity.htmlsafe.event.referenceinsertion;

import org.apache.velocity.Template;
import org.apache.velocity.app.event.ReferenceInsertionEventHandler;
import org.apache.velocity.context.Context;
import org.apache.velocity.context.InternalContextAdapter;
import org.apache.velocity.runtime.resource.Resource;
import org.apache.velocity.util.ContextAware;

import com.atlassian.velocity.htmlsafe.HtmlAnnotationEscaper;
import com.atlassian.velocity.htmlsafe.directive.DefaultDirectiveChecker;
import com.atlassian.velocity.htmlsafe.directive.DirectiveChecker;
import com.atlassian.velocity.htmlsafe.directive.DisableHtmlEscaping;

/**
 * <p>Disables html escaping of references according to the {@link DisableHtmlEscaping} directive.</p>
 * <p>
 * <p>If the directive is present on the template it will not escape the reference;
 * Otherwise, it will delegate escaping of the reference to an {@link HtmlAnnotationEscaper} handler.</p>
 */
public class DisableHtmlEscapingDirectiveHandler implements ReferenceInsertionEventHandler, ContextAware {
    private Context context;
    private DirectiveChecker directiveChecker = new DefaultDirectiveChecker();
    private final ReferenceInsertionEventHandler htmlEscapingHandler;

    public DisableHtmlEscapingDirectiveHandler() {
        htmlEscapingHandler = new HtmlAnnotationEscaper();
    }

    public DisableHtmlEscapingDirectiveHandler(final ReferenceInsertionEventHandler htmlEscapingHandler) {
        this.htmlEscapingHandler = htmlEscapingHandler;
    }

    @Override
    public void setContext(Context context) {
        this.context = context;
    }

    /**
     * Sets the DirectiveChecker that will be used.
     *
     * @param directiveChecker a DirectiveChecker
     */
    public void setDirectiveChecker(DirectiveChecker directiveChecker) {
        this.directiveChecker = directiveChecker;
    }

    @Override
    public Object referenceInsert(String reference, Object value) {
        if (isEscapingDisabled()) {
            return value;
        }
        return htmlEscapingHandler.referenceInsert(reference, value);
    }

    private boolean isEscapingDisabled() {
        if (context instanceof InternalContextAdapter) {
            final InternalContextAdapter ica = (InternalContextAdapter) context;
            final Resource resource = ica.getCurrentResource();
            if (resource instanceof Template) {
                return directiveChecker.isPresent("disable_html_escaping", (Template) resource);
            }
        }
        return false;
    }
}

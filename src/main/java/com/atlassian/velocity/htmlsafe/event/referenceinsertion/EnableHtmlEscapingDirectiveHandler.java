package com.atlassian.velocity.htmlsafe.event.referenceinsertion;

import org.apache.velocity.Template;
import org.apache.velocity.app.event.ReferenceInsertionEventHandler;
import org.apache.velocity.context.Context;
import org.apache.velocity.context.InternalContextAdapter;
import org.apache.velocity.runtime.resource.Resource;
import org.apache.velocity.util.ContextAware;

import com.atlassian.velocity.htmlsafe.HtmlAnnotationEscaper;
import com.atlassian.velocity.htmlsafe.directive.DefaultDirectiveChecker;
import com.atlassian.velocity.htmlsafe.directive.DirectiveChecker;
import com.atlassian.velocity.htmlsafe.directive.EnableHtmlEscaping;

/**
 * <p>Enables html escaping of references according to the {@link EnableHtmlEscaping}
 * directive.</p>
 * <p>
 * <p>If the directive is present on the template it will escape the reference using a {@link HtmlAnnotationEscaper}
 * handler; Otherwise, it will not escape the reference.</p>
 */
public class EnableHtmlEscapingDirectiveHandler implements ReferenceInsertionEventHandler, ContextAware {
    private Context context;
    private DirectiveChecker directiveChecker = new DefaultDirectiveChecker();
    private final ReferenceInsertionEventHandler htmlEscapingHandler;

    public EnableHtmlEscapingDirectiveHandler() {
        this(new HtmlAnnotationEscaper());
    }

    public EnableHtmlEscapingDirectiveHandler(ReferenceInsertionEventHandler htmlEscapingHandler) {
        this.htmlEscapingHandler = htmlEscapingHandler;
    }

    @Override
    public void setContext(Context context) {
        this.context = context;
    }

    /**
     * Sets the DirectiveChecker that will be used.
     *
     * @param directiveChecker a DirectiveChecker
     */
    public void setDirectiveChecker(DirectiveChecker directiveChecker) {
        this.directiveChecker = directiveChecker;
    }

    @Override
    public Object referenceInsert(String reference, Object value) {
        if (isEscapingEnabled()) {
            return htmlEscapingHandler.referenceInsert(reference, value);
        }
        return value;
    }

    private boolean isEscapingEnabled() {
        if (context instanceof InternalContextAdapter) {
            final InternalContextAdapter ica = (InternalContextAdapter) context;
            final Resource resource = ica.getCurrentResource();
            if (resource instanceof Template) {
                return directiveChecker.isPresent("enable_html_escaping", (Template) resource);
            }
        }
        return false;
    }
}

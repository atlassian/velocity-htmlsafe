package com.atlassian.velocity.htmlsafe.introspection;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.List;

import com.google.common.collect.ImmutableSet;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * A method annotator that chains calls to a collection of other annotators
 */
public final class MethodAnnotatorChain implements MethodAnnotator {
    private final MethodAnnotator[] ANNOTATOR_CHAIN;

    public MethodAnnotatorChain(List<MethodAnnotator> annotators) {
        checkNotNull(annotators, "annotators must not be null");
        ANNOTATOR_CHAIN = annotators.toArray(new MethodAnnotator[annotators.size()]);
        // Check these now to aboid an NPE later...
        for (MethodAnnotator annotator : ANNOTATOR_CHAIN) {
            checkNotNull(annotator, "null annotator provided in the list");
        }
    }

    public Collection<Annotation> getAnnotationsForMethod(Method method) {
        ImmutableSet.Builder<Annotation> builder = ImmutableSet.builder();
        for (MethodAnnotator annotator : ANNOTATOR_CHAIN) {
            builder.addAll(annotator.getAnnotationsForMethod(method));
        }
        return builder.build();
    }
}

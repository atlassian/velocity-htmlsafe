package com.atlassian.velocity.htmlsafe.introspection;

import java.lang.annotation.Annotation;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.collect.ImmutableSet;

import com.atlassian.velocity.htmlsafe.annotations.CollectionInheritable;

import static com.google.common.base.Preconditions.checkArgument;

/**
 * An annotated value associates a collection of annotations with a value.
 */
public final class AnnotatedValue<E> implements AnnotationBoxedElement<E> {
    private final E value;
    private final ImmutableSet<Annotation> annotations;

    /**
     * This is a static cache of any annotations that we examine to see if they are annotated as @CollectionInheritable.
     * We are only checking for annotations on annotations here.
     * Annotation classes are, of course, immutable during the life of the JVM, and so this cache is inherently safe.
     * The number of annotations we examine and store in this cache is trivially small.
     **/
    private static final LoadingCache<Annotation, Boolean> annotationCache = CacheBuilder.newBuilder()
            .weakKeys()
            .build(new CacheLoader<Annotation, Boolean>() {
                @Override
                public Boolean load(final Annotation annotation) {
                    return Boolean.valueOf(
                            annotation.annotationType().isAnnotationPresent(CollectionInheritable.class));
                }
            });

    /**
     * Construct a new annotated value.
     *
     * @param value       The value to annotate
     * @param annotations This value's annotations.
     */
    public AnnotatedValue(E value, Collection<Annotation> annotations) {
        checkArgument(!(value instanceof BoxedValue), "Attempting to box an already boxed value");
        this.value = value;
        this.annotations = ImmutableSet.copyOf(annotations);
    }

    public boolean isAnnotationPresent(Class<? extends Annotation> aClass) {
        for (Annotation annotation : annotations) {
            if (annotation.annotationType().equals(aClass)) {
                return true;
            }
        }
        return false;
    }

    public <T extends Annotation> T getAnnotation(Class<T> tClass) {
        for (Annotation annotation : annotations) {
            if (annotation.annotationType().equals(tClass)) {
                return tClass.cast(annotation);
            }
        }

        return null;
    }

    public <T extends Annotation> boolean hasAnnotation(Class<T> tClass) {
        return getAnnotation(tClass) != null;
    }

    public Annotation[] getAnnotations() {
        return annotations.toArray(new Annotation[annotations.size()]);
    }

    public Annotation[] getDeclaredAnnotations() {
        return getAnnotations();
    }

    public Collection<Annotation> getAnnotationCollection() {
        return annotations;
    }

    public E unbox() {
        return value;
    }

    /**
     * This will box another object with the same annotations as this value.
     *
     * @param value Value to box
     * @return Value boxed with the annotations
     */
    public Object box(Object value) {
        return new AnnotatedValue(value, annotations);
    }

    public Collection<Annotation> getCollectionInheritableAnnotations() {
        Collection<Annotation> inheritableAnnotations = new HashSet<Annotation>();
        for (Annotation annotation : annotations) {
            if (annotationCache.getUnchecked(annotation)) {
                inheritableAnnotations.add(annotation);
            }
        }
        return Collections.unmodifiableCollection(inheritableAnnotations);
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final AnnotatedValue<?> that = (AnnotatedValue<?>) o;
        return ((value != null) ? value.equals(that.value) : that.value == null)
                && annotations.equals(that.annotations);
    }

    public int hashCode() {
        int result;
        result = (value != null ? value.hashCode() : 0);
        result = 31 * result + annotations.hashCode();
        return result;
    }

    public final String getDescription() {
        return "Annotated value: " + value.toString() + "; Annotations: " + annotations;
    }

    /**
     * Delegates and returns the result of calling toString on the boxed value. This is unpleasant but necessary as
     * Velocity uses the toString() result when context values are used as part of directive arguments.
     *
     * @return String representation of wrapped value
     */
    public String toString() {
        return value.toString();
    }
}

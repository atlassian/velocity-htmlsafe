package com.atlassian.velocity.htmlsafe.introspection;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Collection;

import com.google.common.collect.ImmutableSet;

import com.atlassian.velocity.htmlsafe.HtmlSafeAnnotationUtils;
import com.atlassian.velocity.htmlsafe.HtmlSafeClassAnnotator;
import com.atlassian.velocity.htmlsafe.HtmlSafeMethodNameAnnotator;

/**
 * Specialisation of the AnnotationBoxingUberspect to be used in a velocity environment.
 * <p>
 * It adds a custom method annotation policy when making a determination on whether a method is HTML safe or not.
 */
public class HtmlSafeAnnotationBoxingUberspect extends AnnotationBoxingUberspect {
    /**
     * A chain of annotators to mark methods safe based on naming conventions and known safe library methods.
     */
    private static final MethodAnnotator HTML_METHOD_ANNOTATOR =
            new MethodAnnotatorChain(Arrays.asList(new HtmlSafeMethodNameAnnotator(), new HtmlSafeClassAnnotator()));

    @Override
    protected Collection<Annotation> getMethodAnnotations(Method method) {
        Collection<Annotation> returnValueAnnotations = super.getMethodAnnotations(method);
        if (returnValueAnnotations.contains(HtmlSafeAnnotationUtils.HTML_SAFE_ANNOTATION)) {
            return returnValueAnnotations;
        }

        // If we haven't determined whether this method call returns a HTML safe value yet, run it past our
        // other annotation policies.
        return ImmutableSet.copyOf(HTML_METHOD_ANNOTATOR.getAnnotationsForMethod(method));
    }
}

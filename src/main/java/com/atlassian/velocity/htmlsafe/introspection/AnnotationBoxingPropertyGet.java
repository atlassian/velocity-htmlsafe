package com.atlassian.velocity.htmlsafe.introspection;

import java.lang.annotation.Annotation;
import java.util.Collection;

import org.apache.velocity.util.introspection.VelPropertyGet;
import com.google.common.collect.ImmutableSet;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Delegating property getter that will box the returned value with a collection of annotations.
 */
final class AnnotationBoxingPropertyGet implements VelPropertyGet {
    private final VelPropertyGet delegate;
    private final Collection<Annotation> annotations;

    public AnnotationBoxingPropertyGet(VelPropertyGet delegate, Collection<Annotation> annotations) {
        this.delegate = checkNotNull(delegate, "delegate must not be null");
        this.annotations = ImmutableSet.copyOf(annotations);
    }

    public Object invoke(Object o) throws Exception {
        final Object obj = delegate.invoke(o);

        if (obj == null) {
            return null;
        }
        return new AnnotatedValue<>(obj, annotations);
    }

    public boolean isCacheable() {
        return delegate.isCacheable();
    }

    public String getMethodName() {
        return delegate.getMethodName();
    }
}

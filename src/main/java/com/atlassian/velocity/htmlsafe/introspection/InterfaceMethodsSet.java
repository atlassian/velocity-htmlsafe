package com.atlassian.velocity.htmlsafe.introspection;

import java.lang.reflect.Method;
import java.util.Set;

import com.google.common.collect.ImmutableSet;

/**
 * Helper class for working with a set of {@link InterfaceMethods}
 */
final class InterfaceMethodsSet {
    private final Set<InterfaceMethods> interfaceMethodsSet;

    InterfaceMethodsSet() {
        interfaceMethodsSet = ImmutableSet.of();
    }

    InterfaceMethodsSet(Set<InterfaceMethods> interfaceMethodsSet) {
        if (interfaceMethodsSet == null) {
            throw new NullPointerException("interfaceMethodsSet");
        }
        this.interfaceMethodsSet = ImmutableSet.copyOf(interfaceMethodsSet);
    }

    /**
     * @return The set of interfaces contained
     */
    public Set<Class<?>> getInterfaces() {
        ImmutableSet.Builder<Class<?>> builder = ImmutableSet.builder();
        for (InterfaceMethods interfaceMethods : interfaceMethodsSet) {
            builder.add(interfaceMethods.getDeclaringInterface());
        }
        return builder.build();
    }

    /**
     * @return The union of all methods contained by the methods set
     */
    public Set<Method> getMethods() {
        ImmutableSet.Builder<Method> builder = ImmutableSet.builder();
        for (InterfaceMethods interfaceMethods : interfaceMethodsSet) {
            builder.addAll(interfaceMethods.getMethods());
        }
        return builder.build();
    }

    public boolean isEmpty() {
        return interfaceMethodsSet.isEmpty();
    }

    /**
     * Return all methods contained in this method set that are implemented by a class
     *
     * @param clazz Class to match
     * @return InterfaceMethodsSet of all matching implemented methods
     */
    public InterfaceMethodsSet getImplementedMethods(Class clazz) {
        ImmutableSet.Builder<InterfaceMethods> implementedMethods = ImmutableSet.builder();
        for (InterfaceMethods methods : interfaceMethodsSet) {
            if (methods.isImplementation(clazz)) {
                implementedMethods.add(methods);
            }
        }
        return new InterfaceMethodsSet(implementedMethods.build());
    }
}

package com.atlassian.velocity.htmlsafe.introspection;

import java.lang.annotation.Annotation;
import java.util.Collection;
import java.util.Iterator;

import com.google.common.collect.ImmutableSet;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * An iterator that will box each value in the iteration with a collection of annotations.
 */
final class AnnotatedValueIterator<E> implements Iterator<AnnotatedValue<E>>, BoxedValue<Iterator<E>> {
    private final Iterator<E> boxedIterator;
    private final Collection<Annotation> annotations;

    public AnnotatedValueIterator(Iterator<E> iterator, Collection<Annotation> annotations) {
        this.boxedIterator = checkNotNull(iterator, "iterator must not be null");
        this.annotations = ImmutableSet.copyOf(annotations);
    }

    public boolean hasNext() {
        return boxedIterator.hasNext();
    }

    public AnnotatedValue<E> next() {
        return new AnnotatedValue<E>(boxedIterator.next(), annotations);
    }

    public void remove() {
        boxedIterator.remove();
    }

    public Iterator<E> unbox() {
        return boxedIterator;
    }
}

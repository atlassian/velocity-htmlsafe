package com.atlassian.velocity.htmlsafe.introspection;

import java.lang.annotation.Annotation;
import java.util.Collection;

import org.apache.velocity.util.introspection.VelMethod;
import com.google.common.collect.ImmutableSet;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Delegating method strategy that will box the returned value with a collection of annotations.
 */
final class AnnotationBoxingMethod implements VelMethod {
    private final VelMethod delegate;
    private final Collection<Annotation> returnValAnnotations;

    AnnotationBoxingMethod(VelMethod delegateMethod, Collection<Annotation> annotations) {
        this.delegate = checkNotNull(delegateMethod, "degateMethod must not be null");
        this.returnValAnnotations = ImmutableSet.copyOf(annotations);
    }

    public Object invoke(Object o, Object[] params) throws Exception {
        final Object obj = delegate.invoke(o, params);
        return (obj == null) ? null : new AnnotatedValue(obj, returnValAnnotations);
    }

    public boolean isCacheable() {
        return delegate.isCacheable();
    }

    public String getMethodName() {
        return delegate.getMethodName();
    }

    public Class<?> getReturnType() {
        return delegate.getReturnType();
    }
}

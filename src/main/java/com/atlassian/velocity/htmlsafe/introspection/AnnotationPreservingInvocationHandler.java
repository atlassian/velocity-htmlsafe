package com.atlassian.velocity.htmlsafe.introspection;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.Set;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Proxy invocation handler that will box the return value of proxied methods with a collection of annotations.
 */
final class AnnotationPreservingInvocationHandler implements InvocationHandler {
    private final Collection<Annotation> annotations;
    private final Object targetObject;
    private final Set<Method> preservingMethods;

    private AnnotationPreservingInvocationHandler(
            Collection<Annotation> annotations, Object targetObject, Set<Method> proxiedMethods) {
        checkArgument(!proxiedMethods.isEmpty(), "proxiedMethods must not be empty");

        this.annotations = annotations;
        this.targetObject = checkNotNull(targetObject, "targetObject must not be null");
        this.preservingMethods = proxiedMethods;
    }

    public AnnotationPreservingInvocationHandler(AnnotationBoxedElement<?> value, Set<Method> preservingMethods) {
        this(value.getAnnotationCollection(), value.unbox(), preservingMethods);
    }

    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        final Object returnValue = method.invoke(targetObject, args);
        return preservingMethods.contains(method) ? new AnnotatedValue(returnValue, annotations) : returnValue;
    }
}

package com.atlassian.velocity.htmlsafe.introspection;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;

import com.atlassian.velocity.htmlsafe.annotations.ReturnValueAnnotation;

/**
 * A method annotator that returns all annotations that have been meta annotated as a {@link ReturnValueAnnotation}
 */
final class ReturnValueAnnotator implements MethodAnnotator {
    private static final Logger log = LoggerFactory.getLogger(ReturnValueAnnotator.class);

    private final LoadingCache<Annotation, Boolean> annotationCache = CacheBuilder.newBuilder()
            .weakKeys()
            .build(new CacheLoader<Annotation, Boolean>() {
                @Override
                public Boolean load(final Annotation annotation) {
                    return Boolean.valueOf(
                            annotation.annotationType().isAnnotationPresent(ReturnValueAnnotation.class));
                }
            });

    public Collection<Annotation> getAnnotationsForMethod(Method method) {
        Collection<Annotation> returnValueAnnotations = new HashSet<>();

        for (Annotation annotation : method.getAnnotations()) {
            if (annotationCache.getUnchecked(annotation)) {
                returnValueAnnotations.add(annotation);
            }
        }
        return Collections.unmodifiableCollection(returnValueAnnotations);
    }
}

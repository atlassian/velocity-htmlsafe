package com.atlassian.velocity.htmlsafe.introspection;

import org.apache.velocity.util.introspection.VelMethod;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * A velocity method that proxies method calls to another object
 */
final class ProxiedMethod implements VelMethod {
    private final VelMethod delegateMethod;
    private final Object proxyObject;

    /**
     * @param delegateMethod method to delegate calls to
     * @param proxyObject    object to receive the method call
     */
    public ProxiedMethod(VelMethod delegateMethod, Object proxyObject) {
        this.delegateMethod = checkNotNull(delegateMethod, "delegateMethod must not be null");
        this.proxyObject = checkNotNull(proxyObject, "proxyObject must not be null");
    }

    public Object invoke(Object o, Object[] params) throws Exception {
        return delegateMethod.invoke(proxyObject, params);
    }

    public boolean isCacheable() {
        return delegateMethod.isCacheable();
    }

    public String getMethodName() {
        return delegateMethod.getMethodName();
    }

    public Class getReturnType() {
        return delegateMethod.getReturnType();
    }
}

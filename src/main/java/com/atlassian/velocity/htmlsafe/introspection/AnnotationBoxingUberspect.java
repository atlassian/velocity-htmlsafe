package com.atlassian.velocity.htmlsafe.introspection;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.velocity.util.RuntimeServicesAware;
import org.apache.velocity.util.introspection.Info;
import org.apache.velocity.util.introspection.Introspector;
import org.apache.velocity.util.introspection.SecureUberspector;
import org.apache.velocity.util.introspection.VelMethod;
import org.apache.velocity.util.introspection.VelPropertyGet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.ImmutableSet;

import com.atlassian.velocity.htmlsafe.annotations.ReturnValueAnnotation;

import static java.util.Collections.emptySet;

/**
 * A Secure Velocity uberspect that boxes return values in an annotated form and unboxes them again when used as arguments
 * to or targets of method calls
 * <p>
 * More specifically this uberspect will inspect any target method call or property for annotations that are marked as
 * {@link ReturnValueAnnotation}s and box the result of calling or accessing the target with these annotations.
 * </p>
 * <p>
 * Since 1.5, this uberspect has also supported deprecation warnings for velocity using deprecated Java classes and
 * methods. These warnings are exceptions in dev mode, but can be disabled using the
 * atlassian.velocity.deprecation.strictmode system property. You should <b>only</b> disable the deprecation warnings if
 * you cannot fix the velocity usage to avoid the deprecations, for backwards compatibility reasons.
 * </p>
 */
public class AnnotationBoxingUberspect extends SecureUberspector implements RuntimeServicesAware {
    private static final Object[] EMPTY_ARRAY = {}; // avoid new Object[]{} calls

    // Do not mask super field with the same name
    private static final Logger logger = LoggerFactory.getLogger(AnnotationBoxingUberspect.class);

    private static final InterfaceMethodsSet ANNOTATION_PRESERVING_METHODS = getAnnotationPreservingCollectionMethods();

    private static final MethodAnnotator RETURN_VALUE_ANNOTATOR = new ReturnValueAnnotator();

    private Introspector basicIntrospector;

    /**
     * Log warnings or debug messages, depending on dev mode, when encountering a deprecation usage in velocity.
     */
    @VisibleForTesting
    static final String ATLASSIAN_DEVMODE = "atlassian.dev.mode";
    /**
     * Enforce or disable throwing exceptions when encountering a deprecation usage in velocity.
     */
    @VisibleForTesting
    static final String ATLASSIAN_VELOCITY_DEPRECATION_STRICTMODE = "atlassian.velocity.deprecation.strictmode";

    /**
     * The old Confluence deprecation switch, only needed while migrating Confluence to using the standard
     * {@link #ATLASSIAN_VELOCITY_DEPRECATION_STRICTMODE}.
     */
    // TODO VHS-30 Remove support for legacy Confluence flag once Confluence upgrades.
    @VisibleForTesting
    static final String LEGACY_CONFLUENCE_VELOCITY_DEPRECATION_STRICTMODE =
            "confluence.velocity.deprecation.strictmode";

    private final ObjectClassResolver CLASS_RESOLVER = this::getClassForTargetObject;

    @Override
    public void init() {
        try {
            super.init();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        basicIntrospector = new Introspector(log, runtimeServices);

        // Add the special annotation value handler so that final rendering of annotated values in the context is made
        // with the correct value string.
        runtimeServices
                .getApplicationEventCartridge()
                .addReferenceInsertionEventHandler(new AnnotatedValueStringHandler());
    }

    /**
     * @return InterfaceMethodsSet representing collection API methods that should inherit the target collection's
     * return value annotations
     */
    private static InterfaceMethodsSet getAnnotationPreservingCollectionMethods() {
        try {
            InterfaceMethods preservingMapMethods = new InterfaceMethods(Map.class.getMethod("get", Object.class));
            InterfaceMethods preservingListMethods = new InterfaceMethods(List.class.getMethod("get", Integer.TYPE));

            Set<InterfaceMethods> preservingMethods = new HashSet<>(2);
            preservingMethods.add(preservingListMethods);
            preservingMethods.add(preservingMapMethods);

            return new InterfaceMethodsSet(preservingMethods);
        } catch (NoSuchMethodException ex) {
            logger.error(
                    "Could not find collection method via reflection. Collection inheritance will not function.", ex);
            return new InterfaceMethodsSet();
        }
    }

    /**
     * Return a method that knows how to unbox method call targets and parameters and to box return values
     * according to the return value boxing policy.
     *
     * @param obj        Object to locate the method on
     * @param methodName Name of the method to locate
     * @param args       Method call arguments
     * @param info       Current template info
     * @return Method calling strategy that will transparently handle boxed arguments and targets while automatically
     * boxing method return values with return value annotations.
     */
    @Override
    public final VelMethod getMethod(Object obj, String methodName, Object[] args, Info info) throws Exception {
        final AnnotatedValueHelper valueHelper = AnnotatedValueHelperFactory.getValueHelper(obj, CLASS_RESOLVER);
        final Object[] unboxedArgs = BoxingUtils.unboxArrayElements(args);

        VelMethod method = super.getMethod(valueHelper.unbox(), methodName, unboxedArgs, info);
        if (method == null) {
            return null;
        }

        method = checkAndGenerateAnnotationPreservingProxy(valueHelper, method, unboxedArgs, info);

        Method refMethod = lookupMethod(methodName, valueHelper, unboxedArgs, info);
        Collection<Annotation> returnValueAnnotations =
                refMethod != null ? getMethodAnnotations(refMethod) : emptySet();
        if (!returnValueAnnotations.isEmpty()) {
            method = new AnnotationBoxingMethod(method, returnValueAnnotations);
        }
        return new UnboxingMethod(method);
    }

    /**
     * Get an iterator responsible for preserving annotations while iterating over a collection that has collection
     * inheritable return value annotations.'
     *
     * @param obj  object to get an iterator for
     * @param info current template info
     * @return Inheritable annotation preserving iterator
     */
    @Override
    public final Iterator<?> getIterator(Object obj, Info info) throws Exception {
        if (!(obj instanceof AnnotatedValue)) {
            return super.getIterator(obj, info);
        }

        AnnotatedValue<?> annotatedValue = (AnnotatedValue<?>) obj;
        Iterator<?> iterator = super.getIterator(annotatedValue.unbox(), info);
        if (iterator == null) {
            return null;
        }

        Collection<Annotation> inheritedAnnotations = annotatedValue.getCollectionInheritableAnnotations();
        if (inheritedAnnotations.isEmpty()) {
            return iterator;
        }
        return new AnnotatedValueIterator<>(iterator, inheritedAnnotations);
    }

    /**
     * Get a property getting strategy that will box the end result with any return value annotations on the property getter
     *
     * @param obj        Object on which a property is being retrieved
     * @param identifier Property identifier
     * @param info       Current template info
     * @return A return value boxing property getter
     */
    @Override
    public final VelPropertyGet getPropertyGet(Object obj, String identifier, Info info) throws Exception {
        final AnnotatedValueHelper valueHelper = AnnotatedValueHelperFactory.getValueHelper(obj, CLASS_RESOLVER);

        VelPropertyGet getter = super.getPropertyGet(valueHelper.unbox(), identifier, info);

        if (getter == null) {
            return null;
        }

        logger.debug("Looking up getter method for annotation resolution: {}", getter.getMethodName());

        Method refMethod = lookupMethod(getter.getMethodName(), valueHelper, EMPTY_ARRAY, info);

        logger.debug("Got method: {}", refMethod);

        if (refMethod == null) {
            return getter;
        }

        Collection<Annotation> annotations = getMethodAnnotations(refMethod);

        logger.debug("Got return annotations: {}", annotations);

        if (annotations.isEmpty()) {
            return getter;
        }

        return new AnnotationBoxingPropertyGet(getter, annotations);
    }

    /**
     * Lookup a method on the target object, logging or throwing deprecation warnings if necessary.
     * <p>
     * We use {@link #basicIntrospector} to avoid unwanted interactions with {@link #introspector} which is a subclass
     * with additional custom resolution logic.
     * <p>
     * Never construct a {@link VelMethod} using the returned value. {@link VelMethod}s should be constructed by
     * leveraging {@link #getMethod} or {@link #introspector} which will additionally apply appropriate security checks.
     */
    protected Method lookupMethod(
            final String methodName,
            final AnnotatedValueHelper valueHelper,
            final Object[] unboxedArgs,
            final Info info) {
        Method targetMethod = basicIntrospector.getMethod(valueHelper.getTargetClass(), methodName, unboxedArgs);

        if (targetMethod != null) {
            logOrThrowDeprecationWarningsIfNecessary(valueHelper, info, targetMethod);
        }

        return targetMethod;
    }

    private void logOrThrowDeprecationWarningsIfNecessary(
            final AnnotatedValueHelper valueHelper, final Info info, final Method targetMethod) {
        final boolean methodDeprecated = targetMethod.isAnnotationPresent(Deprecated.class);
        final boolean classDeprecated = valueHelper.getTargetClass().isAnnotationPresent(Deprecated.class);

        final boolean hasDeprecation = methodDeprecated || classDeprecated;
        final boolean isDevMode = isDevModeEnabled();
        final boolean shouldThrow = shouldThrowExceptionOnDeprecatedMethodAccess();
        final boolean shouldLogWarning = logger.isWarnEnabled() && isDevMode;
        final boolean shouldLogDebug = logger.isDebugEnabled() && !isDevMode;
        final boolean shouldLogOrThrow = shouldThrow || shouldLogDebug || shouldLogWarning;

        if (hasDeprecation && shouldLogOrThrow) {
            final String prefix = classDeprecated
                    ? "Velocity template accessing method on deprecated class"
                    : "Velocity template accessing deprecated method";
            final String error =
                    buildDeprecatedMethodAccessMessage(prefix, valueHelper.getTargetClass(), targetMethod, info);

            if (shouldThrow) {
                throw new UnsupportedOperationException(
                        "(This exception is only thrown when atlassian.velocity.deprecation.strictmode is enabled) "
                                + error);
            }
            if (shouldLogWarning) {
                logger.warn(error);
            }
            if (shouldLogDebug) {
                logger.debug(error);
            }
        }
    }

    static String buildDeprecatedMethodAccessMessage(
            String prefix, Class targetClass, final Method method, final Info info) {
        return String.format(
                "%s %s#%s - %s", prefix, targetClass.getCanonicalName(), method.getName(), info.toString());
    }

    private boolean isDevModeEnabled() {
        return Boolean.getBoolean(ATLASSIAN_DEVMODE);
    }

    private boolean shouldThrowExceptionOnDeprecatedMethodAccess() {
        String strictModeProperty = System.getProperty(ATLASSIAN_VELOCITY_DEPRECATION_STRICTMODE);
        String confluenceStrictModeProperty = System.getProperty(LEGACY_CONFLUENCE_VELOCITY_DEPRECATION_STRICTMODE);

        if (strictModeProperty == null && confluenceStrictModeProperty == null) {
            return false;
        } else if (confluenceStrictModeProperty != null) {
            logger.warn(
                    "System property {} is deprecated. Use {} instead.",
                    LEGACY_CONFLUENCE_VELOCITY_DEPRECATION_STRICTMODE,
                    ATLASSIAN_VELOCITY_DEPRECATION_STRICTMODE);
            return Boolean.valueOf(confluenceStrictModeProperty);
        } else {
            return Boolean.valueOf(strictModeProperty);
        }
    }

    private VelMethod checkAndGenerateAnnotationPreservingProxy(
            AnnotatedValueHelper valueHelper, VelMethod velocityMethod, Object[] unboxedArgs, Info info)
            throws Exception {
        if (!valueHelper.isBoxedValue()) {
            return velocityMethod;
        }

        // Generate a proxy method if annotations are inherited.
        AnnotationBoxedElement<?> annotatedValue = valueHelper.getBoxedValueWithInheritedAnnotations();
        if (annotatedValue != null) {
            return methodProxy(annotatedValue, ANNOTATION_PRESERVING_METHODS, info, unboxedArgs, velocityMethod);
        }

        return velocityMethod;
    }

    /**
     * <p>Return a proxied velocity method if this method call belongs to the set of provided interface methods. The method
     * proxy will be responsible for preserving that annotated value's annotations on any return value from these methods.
     * <p>
     * <p>Invocation proxies are built using the class loader that loaded this uberspect.
     *
     * @param value               Object on which the method is being called
     * @param interfaceMethodsSet Method set defining all methods that inherit annotations
     * @param info                Current template processing info
     * @param unboxedArgs         Unboxed method arguments
     * @param velMethod           Velocity method to proxy if annotations are to be inherited
     * @return A velocity method with the current annotation preserving strategy
     * @throws Exception if the default uberspect implementation does while scanning for this method
     */
    private VelMethod methodProxy(
            AnnotationBoxedElement<?> value,
            InterfaceMethodsSet interfaceMethodsSet,
            Info info,
            Object[] unboxedArgs,
            VelMethod velMethod)
            throws Exception {
        InterfaceMethodsSet implementedInterfaceMethods =
                interfaceMethodsSet.getImplementedMethods(value.unbox().getClass());
        if (implementedInterfaceMethods.isEmpty()) {
            return velMethod;
        }

        final Set<Class<?>> implementedInterfaces = implementedInterfaceMethods.getInterfaces();

        logger.debug("Object implements: {}", implementedInterfaces);

        Object proxiedObject = Proxy.newProxyInstance(
                this.getClass().getClassLoader(),
                implementedInterfaces.toArray(new Class[implementedInterfaces.size()]),
                new AnnotationPreservingInvocationHandler(value, implementedInterfaceMethods.getMethods()));

        VelMethod proxiedMethod = super.getMethod(proxiedObject, velMethod.getMethodName(), unboxedArgs, info);
        if (proxiedMethod == null) {
            // We're not actually calling a method that preserves its annotations. Just return the default method.
            return velMethod;
        }
        logger.debug("Proxying method: {}", proxiedMethod);
        return new ProxiedMethod(proxiedMethod, proxiedObject);
    }

    /**
     * Retrieve any annotations on the supplied method that are meta-annotated as a {@link ReturnValueAnnotation}
     *
     * @param method Method to search
     * @return A collection of annotations that are themselves marked as {@link ReturnValueAnnotation}s.
     */
    protected Collection<Annotation> getMethodAnnotations(Method method) {
        return ImmutableSet.copyOf(RETURN_VALUE_ANNOTATOR.getAnnotationsForMethod(method));
    }

    /**
     * Template method for returning the actual concrete class of a the provided object.
     * <p>
     * This method should be overridden in environments where objects may be proxied in such a way that
     * method annotations are not reflected by the proxying object class.
     * <p>
     * This implementation simply returns <tt>targetObject.getClass()</tt>
     *
     * @param targetObject The object for which the class is being queried
     * @return The actual class that should be queried for return value annotations
     */
    protected Class<?> getClassForTargetObject(Object targetObject) {
        return targetObject.getClass();
    }
}

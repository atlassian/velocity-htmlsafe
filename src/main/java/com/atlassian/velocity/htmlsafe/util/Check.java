package com.atlassian.velocity.htmlsafe.util;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Utility to perform checks on parameters.
 *
 * @deprecated since 1.5. Use {@link com.google.common.base.Preconditions} or {@link java.util.Objects} (Java 8+) instead.
 */
@Deprecated
public final class Check {
    /**
     * Check that {@code reference} is not {@code null}. If it is, throw a
     * {@code NullPointerException}.
     *
     * @param reference reference to check is {@code null} or not
     * @param <T>       reference to check is {@code null} or not
     * @return {@code reference} so it may be used
     * @throws NullPointerException if {@code reference} is {@code null}
     * @deprecated since 1.5. Use {@link com.google.common.base.Preconditions} instead.
     */
    @Deprecated
    public static <T> T notNull(T reference) {
        return checkNotNull(reference);
    }

    /**
     * Check that {@code reference} is not {@code null}. If it is, throw a
     * {@code NullPointerException}.
     *
     * @param reference    reference to check is {@code null} or not
     * @param <T>          reference to check is {@code null} or not
     * @param errorMessage message passed to the {@code NullPointerException} constructor
     *                     to give more context when debugging
     * @return {@code reference} so it may be used
     * @throws NullPointerException if {@code reference} is {@code null}
     * @deprecated since 1.5. Use {@link com.google.common.base.Preconditions} instead.
     */
    @Deprecated
    public static <T> T notNull(T reference, Object errorMessage) {
        return checkNotNull(reference, errorMessage);
    }

    /**
     * Ensures the truth of an expression involving one or more parameters to
     * the calling method.
     *
     * @param expression a boolean expression
     * @throws IllegalArgumentException if {@code expression} is false
     * @deprecated since 1.5. Use {@link com.google.common.base.Preconditions} instead.
     */
    @Deprecated
    public static void argument(boolean expression) {
        checkArgument(expression);
    }

    /**
     * Ensures the truth of an expression involving one or more parameters to
     * the calling method.
     *
     * @param expression   a boolean expression
     * @param errorMessage the exception message to use if the check fails; will be
     *                     converted to a string using {@link String#valueOf(Object)}
     * @throws IllegalArgumentException if {@code expression} is false
     * @deprecated since 1.5. Use {@link com.google.common.base.Preconditions} instead.
     */
    @Deprecated
    public static void argument(boolean expression, Object errorMessage) {
        checkArgument(expression, errorMessage);
    }
}

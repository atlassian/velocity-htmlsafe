package com.atlassian.velocity.htmlsafe.directive;

import org.apache.velocity.Template;
import org.apache.velocity.runtime.parser.node.ASTDirective;
import org.apache.velocity.runtime.parser.node.SimpleNode;
import org.apache.velocity.runtime.visitor.BaseVisitor;

/**
 * Contains a set of static utility methods that operate on Velocity Directives.
 *
 * @see org.apache.velocity.runtime.directive.Directive
 * @since v5.1
 */
public class Directives {
    /**
     * Determines whether a directive with an specified name is present on a template instance.
     *
     * @param directiveName The name of the directive to look for.
     * @param template      The template where we will search for the directive.
     * @return true, if the directive has been defined on the template; otherwise, false.
     */
    public static boolean isPresent(final String directiveName, final Template template) {
        final SimpleNode dataAsSimpleNode = (SimpleNode) template.getData();
        final DirectiveDetectionVisitor isDirectivePresentVisitor = new DirectiveDetectionVisitor(directiveName);
        dataAsSimpleNode.jjtAccept(isDirectivePresentVisitor, new Object());
        return isDirectivePresentVisitor.isPresent();
    }

    private static class DirectiveDetectionVisitor extends BaseVisitor {
        private final String directiveName;
        private boolean present = false;

        public DirectiveDetectionVisitor(final String directiveName) {
            this.directiveName = directiveName;
        }

        public Object visit(ASTDirective node, Object data) {
            if (!present) {
                if (node.getDirectiveName().equals(directiveName)) present = true;
            }

            return true;
        }

        public boolean isPresent() {
            return present;
        }
    }
}

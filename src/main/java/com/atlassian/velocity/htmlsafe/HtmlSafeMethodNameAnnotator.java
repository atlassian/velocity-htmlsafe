package com.atlassian.velocity.htmlsafe;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.Collection;

import com.google.common.collect.ImmutableSet;

import com.atlassian.velocity.htmlsafe.introspection.MethodAnnotator;

/**
 * Method annotator that marks certain methods as being HtmlSafe based on the method name.
 * <p>
 * This policy will annotate any method whose name ends with "Html" or starts with "render" as having a
 * HTML safe return value.
 */
public class HtmlSafeMethodNameAnnotator implements MethodAnnotator {
    public Collection<Annotation> getAnnotationsForMethod(Method method) {
        final String methodName = method.getName();
        if (HtmlSafeAnnotationUtils.endsWithHtmlIgnoreCase(methodName)
                || methodName.startsWith("render")
                || methodName.startsWith("getRender")) {
            return HtmlSafeAnnotationUtils.HTML_SAFE_ANNOTATION_COLLECTION;
        }

        return ImmutableSet.of();
    }
}

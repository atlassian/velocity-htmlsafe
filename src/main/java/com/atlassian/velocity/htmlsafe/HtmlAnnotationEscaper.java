package com.atlassian.velocity.htmlsafe;

import java.lang.annotation.Annotation;
import java.util.Collection;

import com.google.common.collect.ImmutableSet;

import com.atlassian.velocity.htmlsafe.introspection.AnnotatedReferenceHandler;

/**
 * A {@link org.apache.velocity.app.event.ReferenceInsertionEventHandler} that HTML encodes any value not annotated
 * as being HtmlSafe.
 * <p>
 * It also excludes some references from escaping based on name:
 * <ul>
 * <li>Those ending in "html" （case insensitive)
 * <li>xHtmlContent
 * <li>body
 * <li>head
 * </ul>
 */
public class HtmlAnnotationEscaper extends AnnotatedReferenceHandler {
    private static final ImmutableSet<String> SAFE_REFERENCE_NAMES = ImmutableSet.of("xHtmlContent", "body", "head");

    protected Object annotatedValueInsert(String referenceName, Object value, Collection<Annotation> annotations) {
        if (value == null) {
            return null;
        }

        if (shouldEscape(referenceName, value, annotations)) {
            return HtmlEntities.encode(value.toString());
        }

        return value;
    }

    protected boolean shouldEscape(String referenceName, Object value, Collection<Annotation> annotations) {
        final RawVelocityReference reference = new RawVelocityReference(referenceName);

        if (reference.isScalar()) {
            String baseReference = reference.getBaseReferenceName();
            if (HtmlSafeAnnotationUtils.endsWithHtmlIgnoreCase(baseReference)
                    || SAFE_REFERENCE_NAMES.contains(baseReference)) {
                return false;
            }
        }

        return (!(HtmlSafeAnnotationUtils.hasHtmlSafeToStringMethod(value)
                || HtmlSafeAnnotationUtils.containsAnnotationOfType(annotations, HtmlSafe.class)));
    }
}

# Atlassian Velocity HtmlSafe (VHS)

## Description

Atlassian VHS is a library to make or mark values as safe for rendering in HTML strings
when rendered via a [Velocity template][docs-velocity].
The library implements HTML escaping strategies, then provides bindings between those strategies and
a velocity templating engine instance.

## Ownership

This project is owned by the Server Frontend team (go/serverfe).

## Atlassian Developer?

### Committing Guidelines

Please see [The Platform Rules of Engagement (go/proe)](http://go.atlassian.com/proe) for committing to this module.

### Builds

The Bamboo builds for this project are on [EcoBAC][builds].

## External User?

### Issues

Please raise any issues you find with this module in [JIRA][issues].

### Documentation

This library is used in the [Atlassian Template Renderer (ATR)][atr] project to provide out-of-the-box
HTML sanitization. It is recommended to use the template rendering APIs from ATR rather
than consuming this library directly.

Velocity template authors can use directives to enable or disable HTML escaping for their whole template.

1. To explicitly enable HTML escaping, add `#enable_html_escaping()` at the top of your template.
2. To explicitly **disable** HTML escaping, add `#disable_html_escaping()` at the top of your template.

When HTML escaping is enabled for a template, all String and Object values will be assumed unsafe and will be sanitized.

Developers have two main ways to mark Strings and Objects as safe to render as HTML.

1. Add the `@HtmlSafe` annotation to any Java method that will return an already-escaped HTML string value.
2. Use the `HtmlFragment` class to wrap any object with a safe `toString` method.

[atr]: https://bitbucket.org/atlassian/atlassian-template-renderer
[builds]: https://ecosystem-bamboo.internal.atlassian.com/browse/VHS
[docs-velocity]: https://velocity.apache.org/engine/devel/user-guide.html
[issues]: https://ecosystem.atlassian.com/browse/VHS


## Code Quality

This repository enforces the Palantir code style using the Spotless Maven plugin. Any violations of the code style will result in a build failure.

### Guidelines:
- **Formatting Code:** Before raising a pull request, run `mvn spotless:apply` to format your code.
- **Configuring IntelliJ:** Follow [this detailed guide](https://hello.atlassian.net/wiki/spaces/AB/pages/4249202122/Spotless+configuration+in+IntelliJ+IDE) to integrate Spotless into your IntelliJ IDE.


# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

# [5.0.1]
### Fixed
- [CONFSERVER-97879](https://jira.atlassian.com/browse/CONFSERVER-97879) Fixed NPE which occurs when resolving
  annotations on some proxied objects/methods

# [5.0.0]
### Breaking
- Deleted `AllowlistSecureUberspector` and `AllowlistSecureIntrospector` in favour of `SecureUberspector` (in Velocity 1.6.4-atlassian-29 and higher)
- `AnnotationBoxingUberspect` uses new Introspector API introduced in Velocity 1.6.4-atlassian-32 which enables accurate strict allowlisting

# [4.0.5]
### Changed
- removed references to `com.atlassian.annotations.tenancy`

# [4.0.3] - 2023-11-23
### Changed
- [DCPL-559](https://ecosystem.atlassian.net/browse/DCPL-559) - Removed upper limit of the Guava version imported via OSGi, while keeping the lower limit 31. 

# [4.0.2] - 2023-11-16
### Changed
- [DCPL-559](https://ecosystem.atlassian.net/browse/DCPL-559) - Increased upper limit of the Guava version imported via OSGi from 32 to 33, while keeping the lower limit 31.

# [3.2.2-m01] - 2021-12-17
### Changed
Bumped Platform Version to 6.0.0-m04 for Google Guava update
[BSP-3455](https://bulldog.internal.atlassian.com/browse/BSP-3455) Release Atlassian Velocity HTMLSafe with Guava update

# [3.2.2] - 2020-08-10
### Security
Bumping new apache-velocity 1.6.4-atlassian-23 with contain the fix for banned class in sub package

# [3.1.1] - 2020-05-08
### Security
Bringing new velocity to get classsubtype and sub package banning.

## [3.1.0] - 2019-07-26
### Security
Made AnnotationBoxingUberspect a SecureUberspector to restrict dangerous class' usage.
